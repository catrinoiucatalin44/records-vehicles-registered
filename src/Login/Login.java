package Login;

import java.awt.EventQueue;
import dataBase.Database;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import InterfaceApplication.cars;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;

public class Login {

	private JFrame frmEvidentaAutovehiculelor;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmEvidentaAutovehiculelor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEvidentaAutovehiculelor = new JFrame();
		frmEvidentaAutovehiculelor.getContentPane().setBackground(new Color(95, 158, 160));
		frmEvidentaAutovehiculelor.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\catri\\OneDrive\\Desktop\\icons8-car-100.png"));
		frmEvidentaAutovehiculelor.setTitle("EVIDENCE CARS");
		frmEvidentaAutovehiculelor.setBounds(100, 100, 666, 446);
		frmEvidentaAutovehiculelor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEvidentaAutovehiculelor.getContentPane().setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("USER");
		lblNewLabel_1.setIcon(new ImageIcon(""));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(143, 236, 126, 13);
		frmEvidentaAutovehiculelor.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("PASSWORD");
		lblNewLabel_2.setIcon(new ImageIcon(""));
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(143, 280, 126, 13);
		frmEvidentaAutovehiculelor.getContentPane().add(lblNewLabel_2);

		textField = new JTextField();
		textField.setBounds(291, 233, 202, 19);
		frmEvidentaAutovehiculelor.getContentPane().add(textField);
		textField.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(291, 278, 202, 19);
		frmEvidentaAutovehiculelor.getContentPane().add(passwordField);

		JButton btnNewButton_1 = new JButton("LOGIN");
		btnNewButton_1.setIcon(new ImageIcon(""));
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/project", "root", "");
					Statement stmt = conn.createStatement();
					String sql = "Select * from users where User = '" + textField.getText() + "' and Password = '"
							+ passwordField.getText().toString() + "'";
					ResultSet rs = stmt.executeQuery(sql);
					if (rs.next()) {
						JOptionPane.showMessageDialog(null, "LOGIN SUCCESFULLY!");
						cars frame = new cars();
						frame.openWindow();
						frmEvidentaAutovehiculelor.dispose();
					} else
						JOptionPane.showMessageDialog(null, "INCORRECT USERNAME AND PASSWORD!TRY AGAIN!");
					conn.close();

				} catch (Exception e1) {
					System.out.print(e1);
				}
			}
		});
		btnNewButton_1.setBounds(327, 348, 139, 21);
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.setBackground(new Color(47, 79, 79));
		frmEvidentaAutovehiculelor.getContentPane().add(btnNewButton_1);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(327, 65, 139, 184);
		lblNewLabel.setIcon(new ImageIcon(
				""));
		frmEvidentaAutovehiculelor.getContentPane().add(lblNewLabel);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("C:\\\\Users\\\\catri\\\\OneDrive\\\\Desktop\\\\Travel-BMV-icon.png"));
		label.setBounds(67, 26, 231, 170);
		frmEvidentaAutovehiculelor.getContentPane().add(label);
	}
}
