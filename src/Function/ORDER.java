package Function;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.PreparedStatement;

import Model.evidenceCars;
import dataBase.Database;

public class ORDER {

	public ORDER() {
		// TODO Auto-generated constructor stub
	}

	public void array(JTable jTable1) {
		try {
			Connection conn = Database.getDBConnection();
			String query = " Select * from projecttable";
			PreparedStatement pst = (PreparedStatement) conn.prepareStatement(query);
			ResultSet rst = pst.executeQuery();

			ArrayList<evidenceCars> carsArray = new ArrayList<evidenceCars>();

			while (rst.next()) {

				evidenceCars car = new evidenceCars();
				car.setId(rst.getInt("Id"));
				car.setColor(rst.getString("color"));
				car.setDateRegistration(rst.getDate("dateRegistration"));
				car.setEngineCapacity(rst.getDouble("engineCapacity"));
				car.setModel(rst.getString("model"));
				car.setType(rst.getString("type"));
				car.setOwner(rst.getString("owner"));
				car.setRevisionDate(rst.getDate("revisionDate"));
				car.setRegistrationNumber(rst.getString("registrationNumber"));
				carsArray.add(car);
			}
			Collections.sort(carsArray, Comparator.comparing(evidenceCars::getDateRegistration));
			DefaultTableModel table = (DefaultTableModel) jTable1.getModel();
			table.setRowCount(0);
			int i = 0;

			while (i < carsArray.size()) {
				Object object[] = { carsArray.get(i).getId(), carsArray.get(i).getType(), carsArray.get(i).getModel(),
						carsArray.get(i).getColor(), carsArray.get(i).getEngineCapacity(),
						carsArray.get(i).getRegistrationNumber(), carsArray.get(i).getDateRegistration(),
						carsArray.get(i).getRevisionDate(), carsArray.get(i).getOwner() };
				table.addRow(object);
				i++;
			}
		} catch (Exception e) {
			System.out.print(e);
		}
	}
}
