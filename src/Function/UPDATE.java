package Function;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import dataBase.Database;

public class UPDATE {

	public UPDATE() {
		// TODO Auto-generated constructor stub
	}

	public void updateFunction(JTextField txtId, JTextField txtType, JTextField txtModel, JTextField txtColor,
			JTextField txtCapacity, JTextField txtRegistrationNumber, JDateChooser txtDateRegistration,
			JDateChooser txtRevisionDate, JTextField txtOwner) {
		try {
			String sql = "UPDATE projecttable SET type =?, model =?, color =?,"
					+ " engineCapacity =?,registrationNumber =?, dateRegistration =?, revisionDate =?, owner =? WHERE Id =?";
			Connection conn = Database.getDBConnection();
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(9, txtId.getText());
			pst.setString(1, txtType.getText());
			pst.setString(2, txtModel.getText());
			pst.setString(3, txtColor.getText());
			pst.setString(4, txtCapacity.getText());
			pst.setString(5, txtRegistrationNumber.getText());
			java.util.Date utilDate = (java.util.Date) txtDateRegistration.getDate();
			java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
			pst.setDate(6, sqlDate);
			java.util.Date utilDate1 = (java.util.Date) txtRevisionDate.getDate();
			java.sql.Date sqlDate1 = new java.sql.Date(utilDate1.getTime());
			pst.setDate(7, sqlDate1);
			pst.setString(8, txtOwner.getText());
			pst.executeUpdate();

			JOptionPane.showMessageDialog(null, "UPDATED");
			txtId.setText("");
			txtType.setText("");
			txtModel.setText("");
			txtColor.setText("");
			txtCapacity.setText("");
			txtRegistrationNumber.setText("");
			txtDateRegistration.setDate(null);
			txtRevisionDate.setDate(null);
			txtOwner.setText("");
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex);
		}
	}
}
