package InterfaceApplication;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JFrame;
import javax.swing.JPanel;
import net.proteanit.sql.DbUtils;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultComboBoxModel;
import com.toedter.calendar.JDateChooser;

import Function.ADD;
import Function.ORDER;
import Function.UPDATE;
import dataBase.Database;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class cars extends JFrame {

	ResultSet rs = null;
	private JComboBox comboBoxId;
	private JComboBox comboBoxSelect;
	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtType;
	private JTextField txtModel;
	private JTextField txtColor;
	private JTextField txtCapacity;
	private JTextField txtRegistrationNumber;
	private JTextField txtOwner;
	private JTable jTable1;
	private JTextField txtSearch;
	JDateChooser txtDateRegistration = new JDateChooser();
	JDateChooser txtRevisionDate = new JDateChooser();

	/**
	 * Launch the application.
	 */
	public void openWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cars frame = new cars();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void refreshTable() {
		try {
			Connection conn = Database.getDBConnection();
			String query = "SELECT * FROM projecttable";
			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			jTable1.setModel(DbUtils.resultSetToTableModel(rs));

		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "TRY AGAIN!");
		}
	}

	public void fillComboBox() {
		try {
			Connection conn = Database.getDBConnection();
			String query = "SELECT * FROM projecttable";
			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				comboBoxId.addItem(rs.getString("Id"));

			}
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Error");
		}
	}

	/**
	 * Create the frame.
	 */
	public cars() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\catri\\OneDrive\\Desktop\\icons8-car-100.png"));
		setTitle("EVIDENCE CARS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 776, 459);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(95, 158, 160));
		contentPane.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(85, 107, 47)));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Id");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(27, 28, 46, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Type");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_1.setBounds(27, 53, 46, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Model");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_2.setBounds(27, 78, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Color");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_3.setBounds(27, 103, 46, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Capacity");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_4.setBounds(27, 129, 76, 14);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Registration Number");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_5.setBounds(27, 154, 136, 14);
		contentPane.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("Date Registration");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_6.setBounds(27, 179, 136, 14);
		contentPane.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("Revision Date");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_7.setBounds(27, 204, 111, 14);
		contentPane.add(lblNewLabel_7);

		JLabel lblNewLabel_8 = new JLabel("Owner");
		lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_8.setBounds(27, 229, 62, 14);
		contentPane.add(lblNewLabel_8);

		txtId = new JTextField();
		txtId.setEditable(true);
		txtId.setBounds(173, 25, 111, 20);
		contentPane.add(txtId);
		txtId.setColumns(10);

		txtType = new JTextField();
		txtType.setBounds(173, 50, 111, 20);
		contentPane.add(txtType);
		txtType.setColumns(10);

		txtModel = new JTextField();
		txtModel.setBounds(173, 75, 111, 20);
		contentPane.add(txtModel);
		txtModel.setColumns(10);

		txtColor = new JTextField();
		txtColor.setBounds(173, 100, 111, 20);
		contentPane.add(txtColor);
		txtColor.setColumns(10);

		txtCapacity = new JTextField();
		txtCapacity.setBounds(173, 126, 111, 20);
		contentPane.add(txtCapacity);
		txtCapacity.setColumns(10);

		txtRegistrationNumber = new JTextField();
		txtRegistrationNumber.setBounds(173, 151, 111, 20);
		contentPane.add(txtRegistrationNumber);
		txtRegistrationNumber.setColumns(10);

		txtOwner = new JTextField();
		txtOwner.setBounds(173, 226, 111, 20);
		contentPane.add(txtOwner);
		txtOwner.setColumns(10);

		txtDateRegistration.setBounds(173, 173, 111, 20);
		contentPane.add(txtDateRegistration);
		txtRevisionDate.setBounds(173, 198, 111, 20);
		contentPane.add(txtRevisionDate);

		JButton btnNewButton = new JButton("ADD");
		btnNewButton.setIcon(new ImageIcon("C:\\Users\\Radu Emilian\\Desktop\\add.png"));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnNewButton.setBounds(306, 19, 128, 29);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ADD function = new ADD();
				function.addFunction(txtId, txtType, txtModel, txtColor, txtCapacity, txtRegistrationNumber,
						txtDateRegistration, txtRevisionDate, txtOwner);
				refreshTable();
			}
		});
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("DELETE");
		btnNewButton_1.setIcon(new ImageIcon(""));
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnNewButton_1.setBounds(466, 69, 128, 29);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int action = JOptionPane.showConfirmDialog(null, "Do You Really Want To Delete?", "Delete",
						JOptionPane.YES_NO_OPTION);
				if (action == 0) {
					try {

						String sql = "DELETE FROM `projecttable` WHERE Id =?";
						Connection conn = Database.getDBConnection();
						PreparedStatement pst = conn.prepareStatement(sql);
						pst.setString(1, txtId.getText());

						pst.executeUpdate();
						pst.close();

						JOptionPane.showMessageDialog(null, "DELETED");

						txtId.setText("");
						txtType.setText("");
						txtModel.setText("");
						txtColor.setText("");
						txtCapacity.setText("");
						txtRegistrationNumber.setText("");
						txtDateRegistration.setDate(null);
						txtRevisionDate.setDate(null);
						txtOwner.setText("");

					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null, "Id doesn't exists!");
					}
					refreshTable();
				}
			}
		});
		contentPane.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("UPDATE");
		btnNewButton_2.setIcon(new ImageIcon(""));
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnNewButton_2.setBounds(306, 69, 128, 29);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int action = JOptionPane.showConfirmDialog(null, "Do You Really Want To Update This Data?", "Delete",
						JOptionPane.YES_NO_OPTION);
				if (action == 0) {
					UPDATE update = new UPDATE();
					update.updateFunction(txtId, txtType, txtModel, txtColor, txtCapacity, txtRegistrationNumber,
							txtDateRegistration, txtRevisionDate, txtOwner);
					refreshTable();
				}
			}
		});
		contentPane.add(btnNewButton_2);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 301, 718, 108);
		contentPane.add(scrollPane);

		jTable1 = new JTable();
		jTable1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {
					int row = jTable1.getSelectedRow();
					String Id_ = (jTable1.getModel().getValueAt(row, 0)).toString();
					Connection conn = Database.getDBConnection();
					String query = "SELECT * FROM projecttable WHERE Id = '" + Id_ + "'";

					PreparedStatement pst = conn.prepareStatement(query);
					rs = pst.executeQuery();

					while (rs.next()) {
						txtId.setText(rs.getString("Id"));
						txtType.setText(rs.getString("type"));
						txtModel.setText(rs.getString("model"));
						txtColor.setText(rs.getString("color"));
						txtCapacity.setText(rs.getString("engineCapacity"));
						txtRegistrationNumber.setText(rs.getString("registrationNumber"));
						txtDateRegistration.setDate(rs.getDate("dateRegistration"));
						txtRevisionDate.setDate(rs.getDate("revisionDate"));
						txtOwner.setText(rs.getString("owner"));
					}
					pst.close();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		scrollPane.setViewportView(jTable1);
		jTable1.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "ID", "TYPE", "MODEL", "COLOR",
				"ENGINE CAPACITY", "REGISTRATION NUMBER", "DATE REGISTRATION", "REVISION DATE", "OWNER" }));

		JButton btnNewButton_3 = new JButton("RESET");
		btnNewButton_3.setIcon(new ImageIcon(""));
		btnNewButton_3.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtId.setText("");
				txtType.setText("");
				txtModel.setText("");
				txtColor.setText("");
				txtCapacity.setText("");
				txtRegistrationNumber.setText("");
				txtDateRegistration.setDate(null);
				txtRevisionDate.setDate(null);
				txtOwner.setText("");
			}
		});

		btnNewButton_3.setBounds(466, 19, 128, 29);
		contentPane.add(btnNewButton_3);
		
		// LOAD DATA FROM THE DATABASE
		JButton loadDataButton = new JButton("LOAD TABLE DATA");
		loadDataButton.setIcon(new ImageIcon(""));
		loadDataButton.setFont(new Font("Tahoma", Font.BOLD, 17));
		loadDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "SELECT * FROM projecttable";
					Connection conn = Database.getDBConnection();

					PreparedStatement pst = conn.prepareStatement(query);
					rs = pst.executeQuery();
					jTable1.setModel(DbUtils.resultSetToTableModel(rs));
					pst.close();
					rs.close();

				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "TRY AGAIN!");
				}
			}
		});
		loadDataButton.setBounds(323, 247, 254, 43);
		contentPane.add(loadDataButton);

		comboBoxId = new JComboBox();
		comboBoxId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String query = "SELECT * FROM projecttable WHERE Id = ?";
					Connection conn = Database.getDBConnection();
					PreparedStatement pst = conn.prepareStatement(query);
					pst.setString(1, (String) comboBoxId.getSelectedItem());
					rs = pst.executeQuery();

					while (rs.next()) {
						txtId.setText(rs.getString("Id"));
						txtType.setText(rs.getString("type"));
						txtModel.setText(rs.getString("model"));
						txtColor.setText(rs.getString("color"));
						txtCapacity.setText(rs.getString("engineCapacity"));
						txtRegistrationNumber.setText(rs.getString("registrationNumber"));
						txtDateRegistration.setDate(rs.getDate("dateRegistration"));
						txtRevisionDate.setDate(rs.getDate("revisionDate"));
						txtOwner.setText(rs.getString("owner"));
					}
					pst.close();

				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		comboBoxId.setBounds(617, 22, 111, 29);
		contentPane.add(comboBoxId);

		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				try {
					String selection = (String) comboBoxSelect.getSelectedItem();

					String query = "SELECT Id,type,model,engineCapacity,"
							+ "registrationNumber,dateRegistration,revisionDate,owner FROM projecttable WHERE "
							+ selection + " =?";
					Connection conn = Database.getDBConnection();
					PreparedStatement pst = conn.prepareStatement(query);
					pst.setString(1, txtSearch.getText());
					rs = pst.executeQuery();

					jTable1.setModel(DbUtils.resultSetToTableModel(rs));

					while (rs.next()) {

					}

					pst.close();

				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		txtSearch.setBounds(466, 173, 111, 29);
		contentPane.add(txtSearch);
		txtSearch.setColumns(10);

		comboBoxSelect = new JComboBox();
		comboBoxSelect.setModel(new DefaultComboBoxModel(new String[] { "Id", "type", "model", "engineCapacity",
				"registrationNumber", "dateRegistration\t\t", "revisionDate", "owner" }));
		comboBoxSelect.setBounds(323, 173, 117, 29);
		contentPane.add(comboBoxSelect);

		JLabel lblNewLabel_9 = new JLabel("SEARCH DETAILS");
		lblNewLabel_9.setIcon(new ImageIcon(""));
		lblNewLabel_9.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblNewLabel_9.setBounds(370, 129, 189, 34);
		contentPane.add(lblNewLabel_9);

		refreshTable();
		fillComboBox();
		// RESET TEXTFIELDS

		txtId.setText("");
		txtType.setText("");
		txtModel.setText("");
		txtColor.setText("");
		txtCapacity.setText("");
		txtRegistrationNumber.setText("");
		txtDateRegistration.setDate(null);
		txtRevisionDate.setDate(null);
		txtOwner.setText("");

		JButton sortButton = new JButton("SORT");
		sortButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ORDER order = new ORDER();
				order.array(jTable1);
			}
		});

		sortButton.setFont(new Font("Tahoma", Font.BOLD, 17));
		sortButton.setBounds(600, 126, 128, 29);
		contentPane.add(sortButton);
	}
}
