package dataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	static Connection conn = null;

	public Database() {
	}

	public static Connection getDBConnection() {
		try {
			if (conn == null) {
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost/project", "root", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}